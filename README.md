# Sven

![Sven Character](https://i.imgur.com/Gk1EXWI.png)
> _Image by [CJ Boger](https://www.behance.net/gallery/13356499/DOTA-2-Pixel-Art)
> is licensed under [CC BY-NC](https://creativecommons.org/licenses/by-nc/).

Sven manages [Zoomlet](https://gitlab.com/minezoom/zoomlet) Minecraft servers.

## Roadmap

1. Develop REST api
2. Plugin to manager communication
3. Database to persist servers