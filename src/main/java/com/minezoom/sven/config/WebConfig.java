package com.minezoom.sven.config;

import java.util.Objects;

public class WebConfig {
  private String host;
  private int port;
  private SSLConfig ssl;
  private AuthConfig auth;

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    Objects.requireNonNull(host);
    this.host = host;
  }

  public int getPort() {
    return port;
  }

  public void setPort(int port) {
    this.port = port;
  }

  public SSLConfig getSsl() {
    return ssl;
  }

  public void setSsl(SSLConfig ssl) {
    Objects.requireNonNull(host);
    this.ssl = ssl;
  }

  public AuthConfig getAuth() {
    return auth;
  }

  public void setAuth(AuthConfig auth) {
    Objects.requireNonNull(host);
    this.auth = auth;
  }

  public static class SSLConfig {
    private boolean enabled;
    private String keystore;
    private String password;
    private String truststore;
    private String truststorePassword;

    public boolean isEnabled() {
      return enabled;
    }

    public void setEnabled(boolean enabled) {
      this.enabled = enabled;
    }

    public String getKeystore() {
      return keystore;
    }

    public void setKeystore(String keystore) {
      this.keystore = keystore;
    }

    public String getPassword() {
      return password;
    }

    public void setPassword(String password) {
      this.password = password;
    }

    public String getTruststore() {
      return truststore;
    }

    public void setTruststore(String truststore) {
      this.truststore = truststore;
    }

    public String getTruststorePassword() {
      return truststorePassword;
    }

    public void setTruststorePassword(String truststorePassword) {
      this.truststorePassword = truststorePassword;
    }
  }

  /**
   * Todo: Currently does nothing.
   */
  public static class AuthConfig {
    private boolean enabled;

    public boolean isEnabled() {
      return enabled;
    }

    public void setEnabled(boolean enabled) {
      this.enabled = enabled;
    }
  }
}
