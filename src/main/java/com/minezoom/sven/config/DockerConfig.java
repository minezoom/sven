package com.minezoom.sven.config;

import java.util.Objects;
import javax.annotation.Nullable;

public class DockerConfig {
  private String uri;
  private String image;
  private Integer uid;
  private String host;
  private String containerLabel;
  private int containerPort;
  private String containerPath;

  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    Objects.requireNonNull(uri);
    this.uri = uri;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    Objects.requireNonNull(image);
    this.image = image;
  }

  @Nullable
  public Integer getUid() {
    return uid;
  }

  public void setUid(Integer uid) {
    this.uid = uid;
  }

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public String getContainerLabel() {
    return containerLabel;
  }

  public void setContainerLabel(String containerLabel) {
    this.containerLabel = containerLabel;
  }

  public int getContainerPort() {
    return containerPort;
  }

  public void setContainerPort(int containerPort) {
    this.containerPort = containerPort;
  }

  public String getContainerPath() {
    return containerPath;
  }

  public void setContainerPath(String containerPath) {
    Objects.requireNonNull(containerPath);
    this.containerPath = containerPath;
  }
}
