package com.minezoom.sven.config;

public class Config {
  private DockerConfig docker;
  private WebConfig web;

  public DockerConfig getDocker() {
    return docker;
  }

  public void setDocker(DockerConfig docker) {
    this.docker = docker;
  }

  public WebConfig getWeb() {
    return web;
  }

  public void setWeb(WebConfig web) {
    this.web = web;
  }
}
