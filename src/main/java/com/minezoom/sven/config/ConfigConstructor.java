package com.minezoom.sven.config;

import com.google.common.base.CaseFormat;
import java.beans.IntrospectionException;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.introspector.Property;
import org.yaml.snakeyaml.introspector.PropertyUtils;

public class ConfigConstructor extends Constructor {
  public ConfigConstructor() {
    super(Config.class);

    setPropertyUtils(new PropertyUtils() {
      @Override
      public Property getProperty(Class<?> type, String name) throws IntrospectionException {
        if (name.indexOf('-') > -1) {
          name = CaseFormat.LOWER_HYPHEN.to(CaseFormat.LOWER_CAMEL, name);
        }
        return super.getProperty(type, name);
      }
    });
  }
}
