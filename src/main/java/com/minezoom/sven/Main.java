package com.minezoom.sven;

import com.minezoom.sven.config.Config;
import com.minezoom.sven.config.ConfigConstructor;
import com.minezoom.sven.web.WebServer;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import org.yaml.snakeyaml.Yaml;

public class Main {
  public static void main(String[] args) {
    // less spark/jetty logging
    System.setProperty("org.eclipse.jetty.util.log.class", "org.eclipse.jetty.util.log.StdErrLog");
    System.setProperty("org.eclipse.jetty.LEVEL", "INFO");

    Yaml yaml = new Yaml(new ConfigConstructor());
    Config config = yaml.loadAs(Main.class.getResourceAsStream("/config.yml"), Config.class);

    DockerClient docker = DefaultDockerClient.builder()
        .uri(config.getDocker().getUri())
        .build();

    WebServer web = new WebServer();

    Sven sven = new Sven(docker, web);

    try {
      sven.start(config);
    } catch (Exception e) {
      e.printStackTrace();
      System.exit(-1);
    }
  }
}
