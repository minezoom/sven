package com.minezoom.sven.web;

import com.minezoom.sven.config.WebConfig;
import com.minezoom.sven.config.WebConfig.SSLConfig;
import spark.Service;

public class WebServer {
  private Service http;

  public void start(WebConfig config) throws IllegalStateException {
    http = Service.ignite();
    http.ipAddress(config.getHost());
    http.port(config.getPort());
    if (config.getSsl().isEnabled()) {
      SSLConfig ssl = config.getSsl();
      http.secure(
          ssl.getKeystore(),
          ssl.getPassword(),
          ssl.getTruststore(),
          ssl.getTruststorePassword()
      );
    }

    try {
      http.init();
    } catch (Exception e) {
      throw new IllegalStateException("unable to start web server", e);
    }
  }

  public void stop() {
    http.stop();
  }

  public int port() throws IllegalStateException {
    return http().port();
  }

  private Service http() throws IllegalStateException {
    if (http == null)
      throw new IllegalStateException("server not started");
    return http;
  }
}
