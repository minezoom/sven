package com.minezoom.sven.zoomlet;

import com.google.common.collect.ImmutableMap;
import com.minezoom.sven.config.DockerConfig;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import com.spotify.docker.client.messages.ContainerInfo;
import com.spotify.docker.client.messages.HostConfig;
import com.spotify.docker.client.messages.PortBinding;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;

/**
 * A containerized Minecraft server. It may only be started and stopped once.
 */
public class ZoomletServer {
  private final DockerClient docker;
  private final String hostServerPath;
  private String id;

  /**
   * Represents the status of a Zoomlet server container.
   */
  public enum Status {
    NOT_STARTED,
    RUNNING,
    STOPPED
  }

  public ZoomletServer(DockerClient docker, String hostMinecraftPath) {
    this.docker = docker;
    this.hostServerPath = hostMinecraftPath;
  }

  public ZoomletServer(DockerClient docker, String hostMinecraftPath, String id) {
    this(docker, hostMinecraftPath);
    this.id = id;
  }

  /**
   * Start the containerized Minecraft server.
   * @throws DockerException
   * @throws InterruptedException
   * @throws ZoomletServerException if the server is running or has been stopped already
   */
  public void start(DockerConfig config) throws DockerException, InterruptedException, ZoomletServerException {
    if (id != null)
      throw new ZoomletServerException("already started (running? stopped?)");

    // update image
    final String image = config.getImage();
    docker.pull(image);

    HostConfig hostConfig = generateHostConfig(config);
    ContainerConfig containerConfig = generateContainerConfig(config, hostConfig);

    ContainerCreation creation = docker.createContainer(containerConfig);
    id = creation.id();

    docker.startContainer(id);
  }

  /**
   * Stop and remove the docker container if it is running.
   *
   * @throws DockerException
   * @throws InterruptedException
   */
  public void stop() throws DockerException, InterruptedException {
    Status status = status();

    if (status == Status.RUNNING) {
      docker.killContainer(id);
      docker.removeContainer(id);
    }
  }

  /**
   * @return the status of the server
   */
  public Status status() {
    if (id == null)
      return Status.NOT_STARTED;

    try {
      containerInfo();
    } catch (ZoomletServerException e) {
      return Status.NOT_STARTED;
    } catch (DockerException | InterruptedException e) {
      return Status.STOPPED;
    }

    return Status.RUNNING;
  }

  /**
   * Get the host port assigned to the containerized Minecraft server.
   * @return the host port
   * @throws ZoomletServerException if the server has not been started
   * @throws InterruptedException
   * @throws DockerException
   */
  public String port() throws ZoomletServerException, InterruptedException, DockerException {
    ImmutableMap<String, List<PortBinding>> ports = containerInfo().networkSettings().ports();
    if (ports == null)
      throw new IllegalStateException("no port bindings");

    List<PortBinding> bindings = ports.values().iterator().next();
    return bindings.get(0).hostPort();
  }

  private String id() throws ZoomletServerException {
    if (id == null)
      throw new ZoomletServerException("not started");
    return id;
  }

  private ContainerInfo containerInfo()
      throws ZoomletServerException, DockerException, InterruptedException {
    return docker.inspectContainer(id());
  }

  private HostConfig generateHostConfig(DockerConfig config) {
    final String host = config.getHost();
    final String containerPort = String.valueOf(config.getContainerPort());
    final String containerPath = config.getContainerPath();

    PortBinding randomPort = PortBinding.randomPort(host);
    Map<String, List<PortBinding>> portBindings = new HashMap<>();
    portBindings.put(containerPort, Collections.singletonList(randomPort));

    return HostConfig.builder()
        .portBindings(portBindings)
        .binds(hostServerPath + ":" + containerPath)
        .build();
  }

  private ContainerConfig generateContainerConfig(DockerConfig config, HostConfig hostConfig) {
    final String image = config.getImage();
    final Integer uid = config.getUid();
    final String containerLabel = config.getContainerLabel();
    final String containerPort = String.valueOf(config.getContainerPort());

    ContainerConfig.Builder builder = ContainerConfig.builder()
        .hostConfig(hostConfig)
        .labels(ImmutableMap.of(containerLabel, hostServerPath))
        .image(image)
        .exposedPorts(containerPort)
        .tty(true);

    if (uid != null)
      builder.env("USER_ID=" + uid);

    return builder.build();
  }
}
