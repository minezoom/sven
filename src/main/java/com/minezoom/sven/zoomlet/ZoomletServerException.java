package com.minezoom.sven.zoomlet;

public class ZoomletServerException extends Exception {
  public ZoomletServerException(String msg) {
    super(msg);
  }
}
