package com.minezoom.sven;

import com.minezoom.sven.config.Config;
import com.minezoom.sven.config.DockerConfig;
import com.minezoom.sven.config.WebConfig;
import com.minezoom.sven.web.WebServer;
import com.minezoom.sven.zoomlet.ZoomletServer;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.DockerClient.ListContainersParam;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.Container;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Sven {
  private final DockerClient docker;
  private final WebServer web;

  private List<ZoomletServer> servers;
  private Config config;

  public Sven(DockerClient docker, WebServer web) {
    this.docker = docker;
    this.web = web;
  }

  public ZoomletServer startZoomletServer(String minecraftPath) throws IllegalStateException {
    if (docker == null || config == null)
      throw new IllegalStateException("sven not ready");

    ZoomletServer zoomlet = new ZoomletServer(docker, minecraftPath);
    try {
      zoomlet.start(config.getDocker());
    } catch (Exception e) {
      e.printStackTrace();
    }
    return zoomlet;
  }

  public void start(Config config) throws IllegalStateException {
    List<ZoomletServer> servers = new ArrayList<>();

    final DockerConfig dockerConfig = config.getDocker();
    final WebConfig webConfig = config.getWeb();

    try {
      docker.ping();
    } catch (Exception e) {
      throw new IllegalStateException("unable to ping docker", e);
    }

    try {
      String label = dockerConfig.getContainerLabel();
      List<Container> containers = docker.listContainers(ListContainersParam.withLabel(label));

      for (Container container : containers) {
        final String id = container.id();
        final Map<String, String> labels = container.labels();
        if (labels == null)
          continue;

        String hostServerPath = labels.get(label);

        servers.add(new ZoomletServer(docker, id, hostServerPath));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    web.start(webConfig);

    this.config = config;
    this.servers = servers;

    startZoomletServer("/tmp/keenan1");
  }
}
